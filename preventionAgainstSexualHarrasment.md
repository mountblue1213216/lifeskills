# Question 1

**What kinds of behaviour cause sexual harassment?**

# Answer 1:

- Any unwelcome verbal, visual or physical conduct of a sexual nature that is severe or persuasive and affects working conditions or creates a hostile work environment.

- Generally there are 3 forms of sexual harassment behaviour-

1. Verbal
2. Visual
3. Physical

- Normally sexual harassment happens on a work place due to two main reasons

1. Quid Pro Quo(this for that)
2. Hostile work environment

Any sort of sexual harassment should be unacceptable, and immediately reported to the concerned authorities.

# Question 2

**What would you do in case you face or witness any incident or repeated incidents of such behaviour?**

# Answer 2:

- An individual should be very proactive when it comes to sexual harassment, targeted towards in person or towards anyone else in the office community.
- Take note of the details of each incident, including dates, times, locations, and descriptions of what occurred. If possible, gather any available evidence such as screenshots, photos, or videos that can support your account.
- It can be helpful to talk to someone you trust about the incidents you've witnessed or experienced. This could be a friend, family member, or a professional counselor who can provide guidance and support during this time
- Familiarize yourself with the laws and regulations in your jurisdiction regarding the specific behavior or incidents you've encountered. Understanding your rights can empower you to take appropriate action or seek legal advice if needed.
