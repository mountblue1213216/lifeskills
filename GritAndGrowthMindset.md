## 1. Grit

---

### Question 1 Paraphrase (summarize) the video in a few lines. Use your own words.

**Ans.** This video is about how being smart alone, which is measured by something called IQ, doesn't always mean a student will do well in school. What really matters is having something called "grit." Grit means having a strong passion for what you're learning and not giving up easily, even when things get tough. The video says that students who have more grit are more likely to finish school and do really well. The video also talks about how researchers need to do more studies to find out the best ways to help kids develop grit and become successful in school.

### Question 2 What are your key takeaways from the video to take action on?

**Ans.** Here are the key takeaways from the video that you can take action on:

1. It's important to learn from your mistakes and not give up easily. Keep trying different ways to develop a strong passion for what you're learning and persevere in pursuing your long-term goals.

2. Education needs to include a motivational and psychological perspective. This means understanding how to keep students motivated and helping them develop a positive mindset towards learning.

3. Remember that your ability to learn is not fixed or set in stone. It can actually improve if you put in effort and work hard. So don't be discouraged if something seems difficult at first. Keep trying and you'll get better!

## 2. Introduction to Growth Mindset

---

### Question 3 Paraphrase (summarize) the video in a few lines in your own words.

**Ans.** In the video, they talk about two different mindsets that people can have: a fixed mindset and a growth mindset.

A fixed mindset is when someone believes that their abilities and intelligence are fixed and cannot be changed. People with a fixed mindset tend to avoid challenges, effort, making mistakes, and receiving feedback because they think it means they are not smart or talented.

On the other hand, a growth mindset is when someone believes that their abilities can grow and improve through effort and practice. People with a growth mindset see challenges, mistakes, and feedback as opportunities to learn and get better.

It's important to know that these mindsets can vary from person to person and can even change depending on the situation. By understanding the qualities and actions related to each mindset, individuals can figure out where they stand on the spectrum and analyze the beliefs and focus that contribute to their mindset. This can help them develop a growth mindset and be more open to learning and improving.

### Question 4 What are your key takeaways from the video to take action on?

**Ans.**Here are the key takeaway points from the videos:

1. Growth Mindset: The concept of a growth mindset, introduced by Carol Dweck, emphasizes that people's beliefs about their abilities and potential for development greatly influence their ability to learn and succeed.

2. Characteristics of Mindsets: A growth mindset believes that skills can be developed and improved over time. It focuses on the process of learning and becoming better, embraces challenges, mistakes, and feedback as opportunities for growth, and has confidence in the capacity to learn and develop.

3. Importance of Growth Mindset: Having a growth mindset is important because it forms the foundation for effective learning. When individuals believe that they can improve, they are more likely to put in effort and persevere in the face of challenges.

4. Four Key Ingredients to Growth: Effort, challenges, mistakes, and feedback are identified as the key ingredients that contribute to growth and learning. Embracing these elements helps individuals develop their skills and abilities.

5. Creating a Culture for Learning: To create an environment that fosters a culture of learning, it is important to focus on beliefs and mindset. Encouraging a growth mindset among individuals and valuing effort, learning from mistakes, and providing constructive feedback can help create a supportive and conducive learning atmosphere.

By understanding and embracing a growth mindset, individuals can unlock their potential, overcome obstacles, and continuously improve their abilities.

## 3. Understanding Internal Locus of Control

---

### Question 5 What is the Internal Locus of Control? What is the key point in the video?

**Ans** Here are the key points from the video about the internal locus of control:

1. Internal Locus of Control: The internal locus of control is about believing that you have control over your life and that your actions and efforts have an impact on the outcomes you experience.

2. Importance of Internal Locus of Control: The video emphasizes that having an internal locus of control is crucial for maintaining motivation. It means taking responsibility for your own actions and believing that your hard work and effort can lead to success.

3. Study Findings: The video mentions a study that showed individuals who believed their success was a result of their own hard work and effort displayed higher levels of motivation and perseverance. In contrast, those who believed their success was due to external factors, like being naturally smart or gifted, showed lower motivation and were less willing to take on challenging tasks.

4. Benefits of Adopting an Internal Locus of Control: The video suggests that adopting an internal locus of control by taking responsibility for one's actions and achievements can lead to increased motivation and a sense of control over one's life. It highlights the importance of personal effort and actions as driving factors for success.

By embracing an internal locus of control, individuals can feel empowered to take charge of their lives, work hard, and persist in the face of challenges. This mindset can contribute to greater motivation and a belief in one's ability to achieve their goals.

## 3. How to build a Growth Mindset

---

### Question 6 Paraphrase (summarize) the video in a few lines in your own words.

**Ans.**

1. Develop a growth mindset: Believe in your ability to figure things out and improve. This belief in yourself is essential for lifelong growth and improvement.

2. Question your assumptions: Challenge limiting beliefs about yourself and your capabilities. Don't let current knowledge or skills restrict your vision for the future.

3. Create your own life curriculum: Take responsibility for your learning and personal development. Design a curriculum that aligns with your passions and dreams, and actively seek knowledge and skills to pursue them.

4. Honor the struggle: Embrace challenges and setbacks as opportunities for growth. Instead of resisting or giving up, see them as part of the learning process and use them to build resilience.

5. Maintain a long-term growth mindset: By consistently practicing the above principles, you can develop a mindset that is open to change, adaptable, and continually seeks growth and improvement. This mindset enables you to navigate obstacles with confidence and achieve success in various aspects of life.

### Question 7 What are your key takeaways from the video to take action on?

**Ans.**Here are the key takeaways from the video:

1. Believe in Your Ability: Have confidence in your capacity to learn, improve, and overcome challenges. You can figure things out with effort and persistence.

2. Question Your Assumptions: Challenge fixed beliefs or assumptions that may limit your growth. Be open to new possibilities and different ways of thinking.

3. Create Your Own Learning Path: Take responsibility for your personal growth by designing your own learning journey. Set goals, explore new subjects, and pursue interests that inspire you.

4. Embrace Challenges: Rather than avoiding difficulties, embrace them as opportunities for growth. View struggles and setbacks as chances to learn and become better.

5. Cultivate Resilience: Develop resilience by maintaining a positive attitude and persevering in the face of challenges. Believe in your ability to bounce back and keep going.

6. Seek Feedback and Learn from Criticism: Embrace feedback as a valuable tool for growth. Be open to constructive criticism and use it as an opportunity to learn and improve.

By adopting these attitudes and actions, you can cultivate a growth mindset, continuously learn and develop, and overcome obstacles on your path to success.

## 4. Mindset - A MountBlue Warrior Reference Manual

---

### Question 8 What are one or more points that you want to take action on from the manual? (Maximum 3)

**Ans.** Points that I want to take action on from the manual are:

1. I will stay relaxed and focused no matter what happens.
2. I will use the weapons of Documentation, Google, Stack Overflow, Github Issues and Internet before asking help from fellow warriors or look at the solution.
3. I will take ownership of the projects assigned to me. Its execution, delivery and functionality is my sole responsibility.
