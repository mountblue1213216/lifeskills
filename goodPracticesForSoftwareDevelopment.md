# Question 1:

What is your one major takeaway from each one of the 6 sections. So 6 points in total.

1. Make notes while discussing requirements with your team
2. No internet/electricity/laptop not working - Inform relevant team members
3. Use screenshots, diagrams, screencasts tools like loom to show and explain
4. Join the meetings 5-10 mins early to get some time with your team members
5. Remember they have their own work to do as well. Pick and choose your communication medium depending on the situation
6. Remote work is still work.

# Question 2:

Which area do you think you need to improve on? What are your ideas to make progress in that area?

When you talk with your team about what needs to be done, it's important to take notes. Notes are like reminders that help you remember important things. You can write down what everyone says, important ideas, and what you need to do. These notes are helpful because you can look back at them later and remember what you discussed with your team.

Sometimes, things don't work as expected. There might be times when you can't use the internet or your computer stops working. It's important to let your team know about these issues so they can help you. You can talk to them using a phone or other means of communication. It's good to ask for help when you need it, so you can find a solution together.

When you want to show or explain something to your team, there are tools you can use. For example, you can take pictures of your computer screen or make videos to show them what you're talking about. There are special tools that can help you with this, like Loom. These tools make it easier for your team to understand what you're trying to explain, even if you can't be in the same place.

It's a good idea to be a little early for meetings with your team. This gives you some extra time to talk to your team members before the meeting officially starts. You can use this time to chat about things, catch up, or talk about anything urgent. It's a nice way to connect with your team and get ready for the meeting together.

Remember that your team members have their own work to do. They might be busy with other things or have different priorities. So when you need to talk to them, choose the best way to communicate based on the situation. If something is urgent and needs a quick response, you can send a message or make a phone call. If it's something that needs more discussion, you can schedule a meeting or use tools that help you work together, like shared documents or project management software.

Even though you're working from home, it's still work. It's important to take it seriously and do your best. Treat your remote work just like you would treat work in an office. You need to be focused and do your tasks properly. Remember to avoid distractions and create a special place where you can work comfortably. And always try to meet the deadlines and keep up with your responsibilities.
