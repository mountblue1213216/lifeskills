# Question 1

**What are the steps/stratergies to do Active Listening?**

# Answer 1

- Avoid getting distracted by your own thoughts.
- Focus on speaker and topic.
- Try not to interrupt other person.
- Let them finish and then respond.
- Use Door openers, these are phrases that show you are intrested keep the other person talking.
- Show that you are listening with body language.
- Take notes during important conversations.

# Question 2

**According to Fisher's model, what are the key points of Reflective Listening?**

# Answer 2

Reflective listening is a way of listening and talking to others that helps us understand them better and show that we care about what they have to say. Here are some important things to remember about reflective listening:

- Pay attention: When someone is talking to you, look at them and give them your full attention. Try not to get distracted by other things around you.

- Say it in your own words: After the person finishes talking, try to explain what they said using your own words. This shows that you understood what they were saying.

- Understand their feelings: Sometimes, people have strong feelings when they talk. It's important to try and understand how they're feeling and let them know that you understand. For example, if they seem happy or sad, you can say, "It sounds like you're really happy/sad about that."

- Show that you agree: Even if you don't agree with everything the person says, it's still important to show that you understand and respect their point of view. You can say things like, "I can see why you feel that way" or "That's an interesting way to look at it."

- Ask questions: If you want to learn more about what the person is saying, you can ask them open-ended questions. These are questions that can't be answered with just a "yes" or "no." This will help you have a deeper conversation and learn more about each other.

Remember, reflective listening is about listening with kindness and understanding. It helps us build better relationships with others and makes them feel heard and valued.

# Question 3

**What are the obstacles in your listening process?**

# Answer 3

- Lack of attention towards the conversation, due to lack of interest in topic.
- Distractions while having a communication, like regular use of mobile phones during conversation, etc..
- No empathy towards the speaker, showing disregard towards the speaker
- Our resistance to change our views , while listening to the conversation.

# Question 4

**What can you do to improve your listening?**

# Answer 4

Here are a few steps that we can take to improve listening:

1. Pay attention: When someone is talking to you, give them your full attention. Look at them, listen carefully, and don't get distracted by other things around you.

2. Wait your turn: Let the person finish talking before you respond. Don't interrupt or talk over them.

3. Ask questions: If you don't understand something, ask questions to make sure you get it.

4. Show you care: Listen to how the person feels, not just what they say. Pay attention to their emotions and try to understand how they're feeling.

5. Practice being patient: Sometimes, people need time to find the right words or express themselves. Be patient and give them the time they need. It's important to listen and not rush them.

# Quesion 5

**When do you switch to Passive communication style in your day to day life?**

# Answer 5

1. To keep peace: Sometimes, people choose to be passive to avoid arguments or fights. They want everyone to be happy and get along, so they don't say what they really think or feel.

2. To be respectful: When others have different opinions, being passive can show respect. It means listening and letting them talk without interrupting or arguing.

3. To avoid fights: Passive communication can help prevent fights or arguments from getting worse. By not responding with anger or arguing back, it helps keep things calm.

4. To protect feelings: People might be passive to protect someone's feelings. They don't want to say something that could hurt the other person, so they stay quiet even if they disagree.

# Question 6

**When do you switch into Aggressive communication styles in your day to day life?**

# Answer 6

1. When feeling angry or frustrated
2. To assert myself forcefully
3. When feeling threatened or defensive
4. When lacking healthy communication skills, to improvise.

# Question 7

**When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?**

# Answer 7

1. When feeling upset but not saying it directly.
2. When trying to avoid arguments.
3. When wanting attention or control.
4. When feeling afraid of criticism or rejection.

# Question 8

**How can you make your communication assertive?**

# Answer 8

1. Be clear and direct.
2. Use confident body language.
3. Express your feelings.
4. Listen carefully.
5. Believe in yourself.
6. Practice makes perfect.
